def plus_long_plateau(chaine):
    """recherche la longueur du plus grand plateau d'une chaine

    Args:
        chaine (str): une chaine de caractères

    Returns:
        int: la longueur de la plus grande suite de lettres consécutives égales
    """    
    lg_max=0 # longueur du plus grand plateau déjà trouvé
    lg_actuelle=0 #longueur du plateau actuel
    #prec='' #caractère précédent dans la chaine
    for i in range (1,len(chaine)):
        if chaine[i]==chaine[i-1]: # si la lettre actuelle est égale à la précédente
            lg_actuelle+=1
        else: # si la lettre actuelle est différente de la précédente
            if lg_actuelle>lg_max:
                lg_max=lg_actuelle
            lg_actuelle=1
    if lg_actuelle>lg_max: #cas du dernier plateau
        lg_max=lg_actuelle
    return lg_max
print (plus_long_plateau('salut ca va bien toii'))
# --------------------------------------
# Exemple de villes avec leur population
# --------------------------------------

def ville_la_plus_grande (liste_villes,population) :
    """[cherche la ville la plus peuplé]

    Args:
        liste_villes ([list]): [liste de ville]
        population ([list]): [liste de int qui correspondent aux nb d'habitant d'une ville]

    Returns:
        [str]: [retourne la ville la plus peuplé]
    """    
    res = ''
    if len(liste_villes)!= 0 :
        maxi = population[0]
        #La population 
        #
        for i in range(len(liste_villes)) :
            if population[i] > maxi :
                res = liste_villes[i]
                maxi = population[i]
    else :
        return None
    return res 

print (ville_la_plus_grande(["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"],[45869771, 64668,  38426, 43442, 30664, 38250, 22168, 116238, 136463,  25725]))
liste_villes=["Blois","Bourges","Chartres","Châteauroux","Dreux","Joué-lès-Tours","Olivet","Orléans","Tours","Vierzon"]
population=[46532, 64668,  38426, 43442, 30664, 38250, 22168, 45, 136463,  25725]

"""
def test_ville_plus_grande () :
    assert (ville_la_plus_grande(["Teuchiland","Rouen","Paris"],[97,110169,2100000])) == "Paris"
    assert (ville_la_plus_grande([],[])) == None
    assert (ville_la_plus_grande(["Marseille","La Rochelle","Niort"],[800000,200000,70000])) == "Marseille"
    assert (ville_la_plus_grande(["Chartres","Dreux","Arles"],[200000,40000,5000])) == "Chartres"
    """


def chaine_en_nb (chaine) :
    """[Passe d'une chaine de car en int]

    Args:
        chaine ([str]): [Une chaine de caractère]

    Returns:
        [int]: [retourne la chaine en int]
    """
    res = 0
    for i in range(len(chaine)) :
        if chaine[i] == '9' :
            res = res *10+9
        elif chaine[i] == '8' :
            res = res*10+8
        elif chaine[i] == '7' :
            res = res*10+7
        elif chaine[i] == '6' :
            res = res*10+6
        elif chaine[i] == '5' :
            res = res*10+5
        elif chaine[i] == '4' :
            res = res*10+4
        elif chaine[i] == '3' :
            res = res*10+3
        elif chaine[i] == '2' :
            res = res*10+2
        elif chaine[i] == '1' :
            res = res*10+1
        elif chaine[i] == '0' :
            res = res*10
    return res 
print (chaine_en_nb("2010"))




def searc_word (liste_mot,lettre) :
    """[Permet de proposer tous les mots commencant par une lettre passer en parametre]

    Args:
        liste_mot ([list]): [liste de mot]
        lettre ([str]): [une lettre]

    Returns:
        [list]: [Retourne une liste de mot qui correspond a la premiere lettre donne ren parametre]
    """    
    liste_rep = []
    for i in range(len(liste_mot)) :
        if liste_mot[i] != "" :
            if liste_mot[i][0] == lettre :
              liste_rep.append(liste_mot[i])
    return liste_rep
print(searc_word(["yolo","","yakari","allez","youkoulele"],'y'))

def test_seac_word () :
    assert searc_word(["salut","hello","hallo","ciao","hola"],'h') == ["hello","hallo","hola"]
    assert searc_word(["yolo","","yakari","allez","youkoulele"],'y') == ["yolo","yakari","youkoulele"]
    assert searc_word([],"a") == []

def decoupe_mot (phrase) :
    """[Prend une phrase et découpe chaque mot pour la mettre sous forme de liste]

    Args:
        phrase ([str]): [une phrase]

    Returns:
        [list]: [retourne une liste de mot sans les chiffre]
    """    
    liste_mot = []
    mot = ''
    for i in range(len(phrase)) :
        if phrase[i].isalpha() :
            mot += phrase[i]
        else :
            if mot != '':
               liste_mot.append(mot)
            mot = ''
    if mot != '' :
        liste_mot.append(mot)
    return liste_mot
print (decoupe_mot(("salut comment ca va l'artiste ")))

def test_decoupe_mot () :
    assert decoupe_mot("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!") == ['Cela', 'fait', 'déjà', 'jours', 'jours', 'à', 'l', 'IUT', 'O', 'Cool']
    assert decoupe_mot("salut comment ca va l'artiste") == ['salut','comment','ca','va','l','artiste']

def search_word_bis (phrase,lettre) :
    """[Prend une phrase, découpe chaque mot et la met sous forme de liste, puis determine la premiere lettre afin de trouvé chaque mot]

    Args:
        phrase ([str]): [Une phrase]
        lettre ([str]): [une lettre]

    Returns:
        [list]: [Retourne une liste de mot ou la premiere lettre et celle qui est donner en parametre]
    """    
    return searc_word(decoupe_mot(phrase),lettre)
print (search_word_bis("Cela fait déjà 28 jours! 28 jours à l’IUT’O! Cool!!",'C'))

